
using System;

namespace themonospot_Base_Main
{
    /// <summary>
    /// Description of ParserException.
    /// </summary>
    public class ParserException : Exception
    {
        public ParserException()
            : base()
        {            
        }
        
        public ParserException(string Message)
            : base(Message)
        {            
        }
        
        
    }
}
