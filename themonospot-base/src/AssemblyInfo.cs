using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyVersion("0.7.3")]
[assembly: AssemblyTitle("themonospot-base")]
[assembly: AssemblyDescription("Parser/editor and content descriptor for .avi and .mkv file types")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Armando Basile")]
[assembly: AssemblyProduct("themonospot-base")]
[assembly: AssemblyCopyright("Armando Basile")]
[assembly: AssemblyTrademark("None")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]


